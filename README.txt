Git repository to share the TrendAnalysis R package developed during my master thesis project at MeteoSwiss (September 2015 - April 2016).

Different versions are present, you can download only the folder containing the last one.

The folder contents are the following:
- The package .tar.gz source file
- TrendAnalysis.pdf: the package documentation
- TrendAnalysisPres.pdf: a presentation that explains the main concepts of the library, as well as some examples.

Questions? You can write to me at jeanneret.fanny@gmail.com
